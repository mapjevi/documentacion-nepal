##Documentacion NEP@L  

Directorio ppts:  
- Nepal_architecture: Presentación sobre la arquietctura y funcionamiento de NEP@L  
- Programmability: Presentacion Automation & Programmability TIC Forum 2019 Chile y Colombia    
- Programmability �& Services v10: Presentacion TIC ForumArgentina 2019  
- Programability internal: presentaci�n interna  

Directorio documentos:  
- Arquitectura NEPaL: documento técnico sobre NEP@L  
- Principios Automatización y Programabilidad: Conceptos sobre Automatización y Programabilid  
- Cultura de Automatizaci�ón: Documento sobre como promover una cultura de automatizaci�ón     

Directorio papers:  
- Structured_output: uso de las librerias de Python Netmiko y Genie combinadas para obtener salidas estructurada de los comandos  

Directorio imagenes:  
- imagenes sobre NEP@L  


##Instalacion  
1) Acceder a https://gitlab.com/Scrimaglia/documentacion-nepal.git  
2) Click on boton download (seguido a Web IDE)  
3) Descargar como zip (o en el formato preferido)  
4) Descompactar  

